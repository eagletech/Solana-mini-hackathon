use solana_client::{
    rpc_client::RpcClient,
    pubkey::Pubkey,
};
use solana_program::{
    program_pack::Pack,
    system_instruction::create_account,
};
use solana_sdk::{
    commitment_config::CommitmentConfig,
    signature::{Keypair, Signer},
    transaction::Transaction,
    message::Message,
};
use std::str;

// Akıllı sözleşme program ID'si
const PROGRAM_ID: &str = "YOUR_PROGRAM_ID_HERE";

// Solana ağı RPC URL'si
const RPC_URL: &str = "YOUR_RPC_URL_HERE";

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Akıllı sözleşme program kimliği
    let program_id = Pubkey::from_str(PROGRAM_ID)?;

    // Solana bağlantısı oluştur
    let rpc_client = RpcClient::new(RPC_URL.to_string());

    // Özel anahtar ve hesap oluştur
    let payer = Keypair::new();
    let data_account = Keypair::new();

    // Hesap oluştur
    let create_account_instruction = create_account(
        &payer.pubkey(),
        &data_account.pubkey(),
        1000,  // Hesap bakiyesi
        0,    // İşlem ücreti
        &program_id,
    );
    let message = Message::new(&[create_account_instruction]);
    let recent_blockhash = rpc_client.get_recent_blockhash()?.0;
    let mut transaction = Transaction::new(&[&payer], message, recent_blockhash);
    transaction.sign(&[&payer], recent_blockhash);
    rpc_client.send_and_confirm_transaction(&transaction)?;

    // String verisini akıllı sözleşmeye gönder
    let string_data = "Hello, Solana!";
    let instruction_data = format!("set{}", string_data);  // "set" işlemi ve veri birleştirilir
    let instruction_data_bytes = instruction_data.as_bytes();

    let instruction = solana_program::instruction::Instruction {
        program_id,
        accounts: vec![solana_program::instruction::AccountMeta {
            pubkey: data_account.pubkey(),
            is_signer: false,
            is_writable: true,
        }],
        data: instruction_data_bytes.to_vec(),
    };

    let message = Message::new(&[instruction]);
    let recent_blockhash = rpc_client.get_recent_blockhash()?.0;
    let mut transaction = Transaction::new(&[&data_account], message, recent_blockhash);
    transaction.sign(&[&data_account], recent_blockhash);
    rpc_client.send_and_confirm_transaction(&transaction)?;

    // String verisini akıllı sözleşmeden çek
    let instruction_data = "get".to_string();
    let instruction_data_bytes = instruction_data.as_bytes();

    let instruction = solana_program::instruction::Instruction {
        program_id,
        accounts: vec![solana_program::instruction::AccountMeta {
            pubkey: data_account.pubkey(),
            is_signer: false,
            is_writable: true,
        }],
        data: instruction_data_bytes.to_vec(),
    };

    let message = Message::new(&[instruction]);
    let recent_blockhash = rpc_client.get_recent_blockhash()?.0;
    let mut transaction = Transaction::new(&[&data_account], message, recent_blockhash);
    transaction.sign(&[&data_account], recent_blockhash);
    rpc_client.send_and_confirm_transaction(&transaction)?;

    // String verisini oku
    let account_data = rpc_client.get_account(&data_account.pubkey())?;
    let my_data: MyData = MyData::unpack(&account_data.data)?;

    let data_str = str::from_utf8(&my_data.data)?;
    println!("Veri: {}", data_str);

    Ok(())
}