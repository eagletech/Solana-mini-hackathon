// Önce gerekli bağımlılıkları ekleyin
use solana_program::{
    account_info::AccountInfo,
    entrypoint,
    entrypoint::ProgramResult,
    msg,
    pubkey::Pubkey,
    program_error::ProgramError,
    program_pack::Pack,
};
use borsh::BorshDeserialize;
use std::str;

// Akıllı sözleşme programının kimliği
solana_program::declare_id!("YourProgramIdHere");

// Akıllı sözleşme veri yapısı
#[derive(Debug, BorshDeserialize, BorshSerialize)]
pub struct MyData {
    pub data: Vec<u8>,
}

// Akıllı sözleşme işlevi: String atama
pub fn set_string(program_id: &Pubkey, accounts: &[AccountInfo], new_data: String) -> ProgramResult {
    // Hesapların doğruluğunu kontrol et
    let accounts_iter = &mut accounts.iter();
    let my_account = next_account_info(accounts_iter)?;

    // Hesapın program kimliğini ve veri yapısını kontrol et
    if my_account.owner != program_id {
        msg!("Hesap program kimliğiyle uyumsuz");
        return Err(ProgramError::IncorrectProgramId);
    }

    // String verisini u8 türünde bir vektöre dönüştür
    let new_data_bytes = new_data.as_bytes();

    // Veriyi güncelle
    let mut data = MyData {
        data: new_data_bytes.to_vec(),
    };
    data.serialize(&mut &mut my_account.data.borrow_mut()[..])?;

    Ok(())
}

// Akıllı sözleşme işlevi: String okuma
pub fn get_string(accounts: &[AccountInfo]) -> ProgramResult {
    // Hesapların doğruluğunu kontrol et
    let accounts_iter = &mut accounts.iter();
    let my_account = next_account_info(accounts_iter)?;

    // Veriyi oku ve u8 vektörünü stringe dönüştür
    let data = &my_account.data.borrow()[..];
    let data_str = str::from_utf8(data).map_err(|_| ProgramError::InvalidAccountData)?;

    msg!("Veri: {}", data_str);

    Ok(())
}

// İşlem noktası: İşlem başladığında çağrılır
entrypoint!(process_instruction);

// İşlem işlevi
pub fn process_instruction(
    program_id: &Pubkey,
    accounts: &[AccountInfo],
    
    instruction_data: &[u8],
) -> ProgramResult {
    if instruction_data.is_empty() {
        msg!("Boş veri");
        return Err(ProgramError::InvalidInstructionData);
    }

    // İşlem verilerini işle
    let instruction_str = str::from_utf8(instruction_data).map_err(|_| ProgramError::InvalidInstructionData)?;

    // İşlem verilerine göre işlevi çağır
    if instruction_str == "set" {
        let accounts_iter = &mut accounts.iter();
        let my_account = next_account_info(accounts_iter)?;
        let new_data_str = str::from_utf8(&instruction_data[3..]).map_err(|_| ProgramError::InvalidInstructionData)?;
        let new_data = new_data_str.to_string();
        set_string(program_id, accounts, new_data)?;
    } else if instruction_str == "get" {
        get_string(accounts)?;
    } else {
        msg!("Geçersiz işlem");
        return Err(ProgramError::InvalidInstructionData);
    }

    Ok(())
}

// Yardımcı işlev: Sonraki hesabı döndürür veya hata oluşturur
fn next_account_info<'a, 'b>(
    iter: &mut std::slice::Iter<'a, AccountInfo<'b>>,
) -> Result<AccountInfo<'a>, ProgramError> {
    iter.next().ok_or(ProgramError::NotEnoughAccountKeys)
}